//
//  FakeResponseData.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

final class FakeResponseData {

    // MARK: - URL

    static let url: URL = URL(string: "https://myfakeapi.com")!

    // MARK: - Responses

    static let invalidResponse = URLResponse()
    static let response200 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
    static let response401 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 401, httpVersion: nil, headerFields: nil)!
    static let response404 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 404, httpVersion: nil, headerFields: nil)!
    static let response408 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 408, httpVersion: nil, headerFields: nil)!
    static let response429 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 429, httpVersion: nil, headerFields: nil)!
    static let response501 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 501, httpVersion: nil, headerFields: nil)!
    static let response504 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 504, httpVersion: nil, headerFields: nil)!
    static let response598 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 598, httpVersion: nil, headerFields: nil)!
    static let response599 = HTTPURLResponse(url: URL(string: "https://myfakeapi.com")!, statusCode: 599, httpVersion: nil, headerFields: nil)!

    // MARK: - Data

    static var correctData: Data {
        Bundle.module.dataFromResource("data")
    }

    static let incorrectData = "erreur".data(using: .utf8)!
}
