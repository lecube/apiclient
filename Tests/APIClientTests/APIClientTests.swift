//
//  APIClientTests.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import XCTest
@testable import APIClient

final class APIClientTests: XCTestCase {

    // MARK: - Properties

    private let sessionConfiguration: URLSessionConfiguration = {
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        sessionConfiguration.protocolClasses = [URLProtocolFake.self]
        return sessionConfiguration
    }()
    private let request: URLRequest = .init(url: .init(string: "https://myfakeapi.com")!)

    // MARK: - Tests

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenIncorrectResponseIsPassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.invalidResponse)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Request failed")
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenBadResponseIsPassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response501)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Response unsuccessful (status code: 501)")
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse404IsPassed_ThenShouldReturnAClientError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response404)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isClientError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse501IsPassed_ThenShouldReturnAServerError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response501)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isServerError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse408IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response408)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse504IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response504)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse598IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response598)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse599IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response599)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse429IsPassed_ThenShouldReturnATooManyRequestsError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response429)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTooManyRequestsError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenResponse401IsPassed_ThenShouldReturnAnAuthorizationError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response401)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isAuthorizationError())
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenIncorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.incorrectData, FakeResponseData.response200)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(for: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "JSON conversion failed")
        }
    }

    @available(iOS 15.0.0, *)
    func test_GivenLoadForMethod_WhenCorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectResult() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.correctData, FakeResponseData.response200)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let result: [String: String] = try await sut.load(for: request)
            XCTAssertEqual(result["data"], "fakedata")
        } catch {
            XCTFail(#function, file: #file, line: #line)
        }
    }

    func test_GivenLoadFromMethod_WhenIncorrectResponseIsPassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.invalidResponse)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Request failed")
        }
    }

    func test_GivenLoadFromMethod_WhenBadResponseIsPassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response501)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Response unsuccessful (status code: 501)")
        }
    }

    func test_GivenLoadFromMethod_WhenResponse404IsPassed_ThenShouldReturnAClientError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response404)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isClientError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse501IsPassed_ThenShouldReturnAServerError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response501)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isServerError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse408IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response408)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse504IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response504)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse598IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response598)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse599IsPassed_ThenShouldReturnATimeoutError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response599)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTimeoutError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse429IsPassed_ThenShouldReturnATooManyRequestsError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response429)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isTooManyRequestsError())
        }
    }

    func test_GivenLoadFromMethod_WhenResponse401IsPassed_ThenShouldReturnAnAuthorizationError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.response401)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch let error as NSError {
            XCTAssertTrue(error.isAuthorizationError())
        }
    }

    func test_GivenLoadFromMethod_WhenIncorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.incorrectData, FakeResponseData.response200)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let _: [String: String] = try await sut.load(from: request)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "JSON conversion failed")
        }
    }

    func test_GivenLoadFromMethod_WhenCorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectResult() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.correctData, FakeResponseData.response200)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: APIClient = .init(session: fakeSession)

        do {
            let result: [String: String] = try await sut.load(from: request)
            XCTAssertEqual(result["data"], "fakedata")
        } catch {
            XCTFail(#function, file: #file, line: #line)
        }
    }
}
