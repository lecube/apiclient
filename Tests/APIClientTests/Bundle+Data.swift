//
//  Bundle+Data.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

extension Bundle {

    /**
    Extract data to a json file
    
    - parameter resource: name of the json file.
    - returns: data
    */
    func dataFromResource(_ resource: String) -> Data {
        guard let mockURL = url(forResource: resource, withExtension: "json"),
              let data = try? Data(contentsOf: mockURL) else {
                 fatalError("Failed to load \(resource) from bundle.")
        }
        return data
    }
}
