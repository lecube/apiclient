//
//  APIErrorProviderTests.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import XCTest
@testable import APIClient

final class APIErrorProviderTests: XCTestCase {

    // MARK: - Tests

    func test_GivenAnErrorCode101_WhenCallingError_ThenShouldReturnTheCorrectError() {
        let error = APIErrorProvider.error(forCode: APIErrorCode.requestFailed.rawValue)

        XCTAssert(error.domain == "com.lemecanisme.apiclient")
        XCTAssert(error.code == 001)
        XCTAssert(error.userInfo[NSLocalizedDescriptionKey] as! String == "Request failed")
        XCTAssert(error.userInfo[NSLocalizedFailureReasonErrorKey] as! String == "Request failed")
    }

    func test_GivenAnErrorCode102_WhenCallingError_ThenShouldReturnTheCorrectError() {
        let error = APIErrorProvider.error(forCode: 404)

        XCTAssert(error.domain == "com.lemecanisme.apiclient")
        XCTAssert(error.code == 404)
        XCTAssert(error.userInfo[NSLocalizedDescriptionKey] as! String == "Response unsuccessful (status code: 404)")
        XCTAssert(error.userInfo[NSLocalizedFailureReasonErrorKey] as! String == "Response unsuccessful (status code: 404)")
    }

    func test_GivenAnErrorCode103_WhenCallingError_ThenShouldReturnTheCorrectError() {
        let error = APIErrorProvider.error(forCode: APIErrorCode.jsonConversionFailed.rawValue)

        XCTAssert(error.domain == "com.lemecanisme.apiclient")
        XCTAssert(error.code == 002)
        XCTAssert(error.userInfo[NSLocalizedDescriptionKey] as! String == "JSON conversion failed")
        XCTAssert(error.userInfo[NSLocalizedFailureReasonErrorKey] as! String == "JSON conversion failed")
    }

    func test_GivenAnUnknownErrorCode_WhenCallingError_ThenShouldReturnTheCorrectError() {
        let error = APIErrorProvider.error(forCode: 011)

        XCTAssert(error.domain == "com.lemecanisme.apiclient")
        XCTAssert(error.code == 011)
        XCTAssert(error.userInfo[NSLocalizedDescriptionKey] as! String == "")
        XCTAssert(error.userInfo[NSLocalizedFailureReasonErrorKey] as! String == "")
    }
}
