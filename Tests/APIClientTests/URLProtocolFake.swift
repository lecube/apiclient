//
//  URLProtocolFake.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

final class URLProtocolFake: URLProtocol {
    static var fakeURLs = [URL?: (data: Data?, response: URLResponse?)]()

    override class func canInit(with request: URLRequest) -> Bool { true }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest { request }

    override func startLoading() {
        if let url = request.url {
            if let (data, response) = URLProtocolFake.fakeURLs[url] {
                if let responseStrong = response {
                    client?.urlProtocol(self, didReceive: responseStrong, cacheStoragePolicy: .notAllowed)
                }
                if let dataStrong = data {
                    client?.urlProtocol(self, didLoad: dataStrong)
                }
            }
        }
        client?.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() { }
}
