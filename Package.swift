// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "APIClient",
    platforms: [.iOS("13.2")],
    products: [
        .library(name: "APIClient", targets: ["APIClient"]),
    ],
    dependencies: [
        .package(name: "ErrorUtilities", url: "https://bitbucket.org/lecube/errorutilities", branch: "main")
    ],
    targets: [
        .target(name: "APIClient", dependencies: ["ErrorUtilities"]),
        .testTarget(name: "APIClientTests", dependencies: ["APIClient"], resources: [.process("data.json")]),
    ]
)
