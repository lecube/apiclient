//
//  APIErrorProvider.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation
import ErrorUtilities

enum APIErrorCode: ErrorCode {
    case requestFailed = 001
    case jsonConversionFailed = 002
}

struct APIErrorProvider: ErrorProvider {

    // MARK: - Properties

    static let domain = "com.lemecanisme.apiclient"

    // MARK: - Error

    static func error(forCode code: ErrorCode) -> NSError {
        var userInfo = [String: Any]()
        userInfo[NSLocalizedDescriptionKey] = localizedDescriptionForCode(code: code)
        userInfo[NSLocalizedFailureReasonErrorKey] = failureReasonForCode(code: code)
        return NSError(domain: domain, code: code, userInfo: userInfo)
    }

    // MARK: - Helpers

    private static func localizedDescriptionForCode(code: ErrorCode) -> String {
        switch code {
        case APIErrorCode.requestFailed.rawValue:
            return "Request failed"
        case 100 ... 600:
            return "Response unsuccessful (status code: \(code))"
        case APIErrorCode.jsonConversionFailed.rawValue:
            return "JSON conversion failed"
        default:
            return .init()
        }
    }

    private static func failureReasonForCode(code: ErrorCode) -> String {
        switch code {
        case APIErrorCode.requestFailed.rawValue:
            return "Request failed"
        case 100 ... 600:
            return "Response unsuccessful (status code: \(code))"
        case APIErrorCode.jsonConversionFailed.rawValue:
            return "JSON conversion failed"
        default:
            return .init()
        }
    }
}
