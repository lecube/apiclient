//
//  AsyncAPIClient.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

public protocol AsyncAPIClient {

    // MARK: - Actions

    @available(iOS 15.0.0, *)
    func load<T: Decodable>(for request: URLRequest) async throws -> T

    @available(iOS, deprecated: 15.0, message: "Use the built-in API instead")
    func load<T: Decodable>(from request: URLRequest) async throws -> T
}
