//
//  URLSession+Async.swift
//  
//
//  Created by Sebastien Bastide on 28/12/2021.
//

import Foundation

extension URLSession {

    /**
    Make an Async / Await request for iOS 13.2 <-> 15.0
    
    - parameter resource: the url request.
    - returns: (Data, URLResponse)
    */
    @available(iOS, deprecated: 15.0, message: "Use the built-in API instead")
    func data(from request: URLRequest) async throws -> (Data, URLResponse) {
        try await withCheckedThrowingContinuation { continuation in
            let task = dataTask(with: request) { data, response, error in
                guard let data = data, let response = response else {
                    let error = error ?? URLError(.badServerResponse)
                    return continuation.resume(throwing: error)
                }
                continuation.resume(returning: (data, response))
            }
            task.resume()
        }
    }
}
