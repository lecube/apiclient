//
//  APIClient.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

public struct APIClient: AsyncAPIClient {

    // MARK: - Properties

    private let session: URLSession
    private let decoder: JSONDecoder = .init()

    // MARK: - Initializer

    public init(session: URLSession = .init(configuration: .default)) {
        self.session = session
    }

    /**
    Make a generic Async / Await request for iOS  15.0+
    
    - parameter resource: the url request.
    - returns: (Data, URLResponse)
    */
    @available(iOS 15.0.0, *)
    public func load<T: Decodable>(for request: URLRequest) async throws -> T {
        let (data, response) = try await session.data(for: request)
        try responseHandler(with: response)
        guard let jsonDecoded = try? decoder.decode(T.self, from: data) else {
            throw APIErrorProvider.error(forCode: APIErrorCode.jsonConversionFailed.rawValue)
        }
        return jsonDecoded
    }

    /**
    Make a generic Async / Await request for iOS  13.2 <-> 15.0
    
    - parameter resource: the url request.
    - returns: (Data, URLResponse)
    */
    @available(iOS, deprecated: 15.0, message: "Use the built-in API instead")
    public func load<T: Decodable>(from request: URLRequest) async throws -> T {
        let (data, response) = try await session.data(from: request)
        try responseHandler(with: response)
        guard let jsonDecoded = try? decoder.decode(T.self, from: data) else {
            throw APIErrorProvider.error(forCode: APIErrorCode.jsonConversionFailed.rawValue)
        }
        return jsonDecoded
    }

    // MARK: - Helpers

    private func responseHandler(with response: URLResponse) throws {
        guard let response = response as? HTTPURLResponse else {
            throw APIErrorProvider.error(forCode: APIErrorCode.requestFailed.rawValue)
        }
        guard response.statusCode == 200 else {
            throw APIErrorProvider.error(forCode: response.statusCode)
        }
    }
}
